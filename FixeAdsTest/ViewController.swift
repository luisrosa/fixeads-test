//
//  ViewController.swift
//  FixeAdsTest
//
//  Created by Luís Carlos Rosa on 02/11/15.
//  Copyright © 2015 Personal. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPageViewControllerDataSource {

    let defaults = NSUserDefaults.standardUserDefaults()
    
    private var pageViewController: UIPageViewController?
    @IBOutlet weak var tableView: UITableView!
    var ads = NSMutableOrderedSet()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if let savedAds = NSUserDefaults.standardUserDefaults().objectForKey("ads") as? [NSDictionary]{
            self.ads.addObjectsFromArray(savedAds)
        }
        Alamofire.request(.GET, "https://olx.pt/i2/ads/?json=1&search[category_id]=25").validate().responseJSON(options: .AllowFragments) { (response) -> Void in
            
            let JSON = response.result.value
            switch response.result {
            case .Success:
                let adsInfo = (JSON as! NSDictionary).valueForKey("ads") as! [NSDictionary]
                self.ads.addObjectsFromArray(adsInfo)
                self.defaults.setObject(adsInfo, forKey: "ads")
                self.tableView.reloadData()
            case .Failure(let error):
                print("Error: \(error)")
                
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ads.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("AdCell", forIndexPath: indexPath)
        cell.textLabel?.text = ads.objectAtIndex(indexPath.row).objectForKey("title") as? String
        return cell
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let shareRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.Normal, title: NSLocalizedString("Partilhar", comment: ""), handler:{action, indexpath in
            let textToShare = self.ads.objectAtIndex(indexPath.row).objectForKey("title") as! String
            
            if let myWebsite = NSURL(string: (self.ads.objectAtIndex(indexPath.row).objectForKey("url") as? String)!)
            {
                let objectsToShare = [textToShare, myWebsite]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                
                
                self.presentViewController(activityVC, animated: true, completion: nil)
            }
            
        });
        shareRowAction.backgroundColor = UIColor.orangeColor()
        
        
        return [shareRowAction];
    }

    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let pageController = segue.destinationViewController as! UIPageViewController
        pageController.dataSource = self
        let indexPath = self.tableView.indexPathForSelectedRow
        self.tableView.deselectRowAtIndexPath(indexPath!, animated: true)
        if ads.count > 0 {
            let firstController = getItemController((indexPath?.row)!)!
            let startingViewControllers: NSArray = [firstController]
            pageController.setViewControllers(startingViewControllers as? [UIViewController], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
        }
    }
    
    // MARK: - UIPageViewControllerDataSource
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! DetailViewController
        
        if itemController.itemIndex > 0 {
            return getItemController(itemController.itemIndex-1)
        }
        
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! DetailViewController
        
        if itemController.itemIndex+1 < self.ads.count {
            return getItemController(itemController.itemIndex+1)
        }
        
        return nil
    }
    
    private func getItemController(itemIndex: Int) -> DetailViewController? {
        
        if itemIndex < self.ads.count {
            let pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("DetailViewController") as! DetailViewController
            pageItemController.itemIndex = itemIndex
            pageItemController.ad = self.ads.objectAtIndex(itemIndex) as? NSDictionary
            return pageItemController
        }
        
        return nil
    }

}

