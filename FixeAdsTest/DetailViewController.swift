//
//  DetailViewController.swift
//  FixeAdsTest
//
//  Created by Luís Carlos Rosa on 02/11/15.
//  Copyright © 2015 Personal. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    var itemIndex: Int = 0
    var ad:NSDictionary?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionTextField: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let title = ad?.objectForKey("title") as? String {
            self.titleLabel.text = title
        }
        if let dateString = ad?.objectForKey("created") as? String {
            self.dateLabel.text = dateString
        }
        if let descriptionString = ad?.objectForKey("description") as? String {
            self.descriptionTextField.text = descriptionString
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowMap" {
            let mapViewController = segue.destinationViewController as! MapViewController
            mapViewController.latitude = Double((ad?.objectForKey("map_lat") as? String)!)
            mapViewController.longitude = Double((ad?.objectForKey("map_lon") as? String)!)
            mapViewController.mapZoom = ad?.objectForKey("map_zoom") as? Double
            mapViewController.city = ad?.objectForKey("city_label") as? String
        }
        
    }
    

}
